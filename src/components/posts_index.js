import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
// basically a tags for react - doesnt physically go anywhere just will trigger a new view to load.
import { Link } from 'react-router-dom';
//pull in the action creator
import { fetchPosts } from '../actions/index';

class PostsIndex extends Component {

    componentDidMount() {
      this.props.fetchPosts();  
    }

    renderPosts() {
        //lodash can map objects - you cant map an object like array - by default map only accessible on arrays
        return _.map(this.props.posts, post => {
            return (
                <li className="list-group-item" key={post.id}>
                    <Link to={`/posts/${post.id}`}>
                        {post.title}
                    </Link>
                </li>
            );
        });
    }
    

    render() {
        return(
            <div>
                <div className="text-xs-right">
                    <Link className="btn btn-primary" to="/posts/new">Add a Post</Link>
                </div>
                <h3>Current Posts</h3>
                <ul className="list-group">
                    {this.renderPosts()}
                </ul>
            </div>
        );
    }
}

//get state which will hold the get request data
function mapStateToProps(state) {
    return { posts: state.posts };
}

// can use object here - save using match dispatch to props
export default connect(mapStateToProps, { fetchPosts })(PostsIndex);