import React, { Component } from 'react';
//reduxForm function makes the form connected to redux store basically. similar to connect helper.
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPost } from '../actions/index';

class PostNew extends Component {
    renderField(field) {

        // destructuring
        const { meta : { touched, error } } =  field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return(
            <div className={className}>
                <label>{field.label}</label>
                <input className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    // helper function I define
    onSubmit(values) {
        //post the values up to the API
        this.props.createPost(values, () => {
            // redirect is a callback function
            this.props.history.push('/');
        });
    }

    render() {
        const { handleSubmit } = this.props;

        return(
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field 
                    label="Title"
                    name="title"
                    //visual piece
                    component={this.renderField}
                />

                <Field 
                    label="Categories"
                    name="categories"
                    component={this.renderField}
                />

                <Field 
                    label="Post Content"
                    name="content"
                    component={this.renderField}
                />
                <button type="submit" className="btn btn-primary">Submit Post</button>
                <Link to="/" className="btn btn-danger">Cancel</Link>
            </form>
        );
    }
}

// will be called on submit automatically via reduxform
// if we return empty errors object form is fine to submit - if it has properties in the errors obj it will not submit
function validate(values) {
    const errors = {};
    
    //validate inputs
    if (!values.title) {
        errors.title = "Enter a title!";
    }

    if (!values.categories) {
        errors.categories = "Enter a category!";
    }

    if (!values.content) {
        errors.content = "Enter some content please!";
    }

    return errors;
}

// using connect and redux form - looks harder - mutiple helpers used.
export default reduxForm({
    validate,
    form: 'PostNewForm'
})(
    connect(null, { createPost }) (PostNew)
);