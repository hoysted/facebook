import { combineReducers } from 'redux';
// package basically saves us creating action creators for each form element - saves some wiring up
// automatically updates stet based on form updates - saves loads of bolerplate code
import { reducer as formReducer } from 'redux-form';
import PostsReducer from './reducer_posts';

const rootReducer = combineReducers({
  posts: PostsReducer,
  form: formReducer
});

export default rootReducer;
