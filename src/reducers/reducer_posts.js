import { FETCH_POSTS, FETCH_POST, DELETE_POST } from '../actions/';
import _ from 'lodash';

export default function (state = {}, action) {
    switch (action.type) {
    case DELETE_POST:
        // returns new state obj with this post id not present any more
        // obj for state management is way easier
        return _.omit(state, action.payload);
    case FETCH_POST:
        // const post = action.payload.data;
        // const newState = { ...state, }
        // newState[post.id] = post;
        // return newState;
        // ley interpolation - new object on state with id as the key to the data.
        // this will build up state piece by piece if user goes from index to post back and forth
        return { ...state, [action.payload.data.id]: action.payload.data };
    case FETCH_POSTS:
        return _.mapKeys(action.payload.data, 'id');
        
    default:
        return state;
    }
}